const picker = new easepick.create({
    element: document.getElementById('datepicker'),
    css: [
        'https://cdn.jsdelivr.net/npm/@easepick/bundle@1.2.1/dist/index.css',
        'https://easepick.com/css/customize_sample.css',
    ],
    zIndex: 10,
    plugins: [
        "RangePlugin",
        "AmpPlugin"
    ],
    format: 'DD.MM.YY',
});
