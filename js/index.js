/* Sub form */

const formInput = document.querySelector('.sub_form-input');
const formLabel = document.querySelector('.sub_form-label');

formInput.addEventListener('input', () => {
    if (formInput.value.trim() != '') {
        formLabel.classList.add('sub_form-label--top');
    } else {
        formLabel.classList.remove('sub_form-label--top');
    }
})

/* burger menu */

const mobileBtn = document.querySelector('.mobile_nav-btn');
const mobileNav = document.querySelector('.mobile_nav');

$(function() {
    $('#mobile_nav-btn').click(function(){
        $(this).toggleClass('open');
        mobileNav.classList.toggle('mobile_nav--open');
    });
});